﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GothamCrimProject.DATA
{
    class CrimeReportStatusMD
    {
        public int CrimeStatusID { get; set; }
        [Display(Name ="Status of Crime")]
        public string CrimeStatus { get; set; }
        [Display(Name ="Description")]
        [UIHint("MultilineText")]
        public string CrimeStatusDescription { get; set; }
    }
    [MetadataType(typeof(CrimeReportStatusMD))]
    public partial class GCCrimeReportStatu { }
}
