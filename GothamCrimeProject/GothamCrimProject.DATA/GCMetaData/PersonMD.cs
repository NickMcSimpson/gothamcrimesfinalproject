﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GothamCrimProject.DATA
{
    class PersonMD
    {
        public int PersonID { get; set; }
        [Display(Name ="First Name")]
        public string PersonFName { get; set; }
        [Display(Name ="Last Name")]
        public string PersonLName { get; set; }
        [Display(Name = "Alias")]
        public string PersonAlias { get; set; }
        [Display(Name = "Phone Number")]
        public string ContactPhone { get; set; }
        [Display(Name = "Location")]
        public int PersonLocationID { get; set; }
        [Display(Name = "Affiliation")]
        public int AffiliationID { get; set; }
        [Display(Name = "Occupation")]
        public string Occupation { get; set; }
        [Display(Name = "Status")]
        public int PersonStatusID { get; set; }
        [Display(Name = "Notable Features")]
        [UIHint("MultilineText")]
        public string PersonNotableFeatures { get; set; }
        [Display(Name = "Trust Rating")]
        public Nullable<byte> TrustRating { get; set; }
        public string PersonImage { get; set; }
        [Display(Name = "Email")]
        public string PersonEmail { get; set; }
    }
    [MetadataType(typeof(PersonMD))]
    public partial class GCPerson {
        public string FullName
        {
            get
            {
                return PersonFName + " " + PersonLName;
            }
        }
        public string AliasOr
        {
            get
            {
                if(PersonAlias != null)
                {
                    return PersonAlias;
                }
                else
                {
                    return FullName;
                }
            }
        }
    }//end partial Person class
}
