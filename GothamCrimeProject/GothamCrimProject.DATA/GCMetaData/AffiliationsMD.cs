﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GothamCrimProject.DATA
{
    class AffiliationsMD
    {
        public int AffiliationID { get; set; }
        [Display(Name ="Affiliation")]
        public string AffilitationName { get; set; }
        [Display(Name = "Description")]
        [UIHint("MultilineText")]
        public string AffilitationDescription { get; set; }
    }
    [MetadataType(typeof(AffiliationsMD))]
    public partial class GCAffiliation { }
}
