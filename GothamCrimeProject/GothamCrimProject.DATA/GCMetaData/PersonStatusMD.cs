﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GothamCrimProject.DATA
{
    class PersonStatusMD
    {
        public int PersonStatusID { get; set; }
        [Display(Name ="Status")]
        public string PersonStatusName { get; set; }
        [Display(Name ="Status Description")]
        [UIHint("MultilineText")]
        public string PersonStatusDescription { get; set; }
    }
    [MetadataType(typeof(PersonStatusMD))]
    public partial class GCPersonStatu { }
}
