﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GothamCrimProject.DATA
{
    class CrimeReportNoteMD
    {
        public int CrimeReportNoteID { get; set; }
        [Display(Name ="Note From")]
        public int CRNAuthorID { get; set; }
        [Display(Name = "Bat-Note")]
        [UIHint("MultilineText")]
        public string CRNote { get; set; }
        [Display(Name = "Linked Crime")]
        public int CRNCrimeID { get; set; }
        [Display(Name = "Note Posted")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public System.DateTime CRNDate { get; set; }
    }
    [MetadataType(typeof(CrimeReportNoteMD))]
    public partial class GCCrimeReportNote { }
}
