﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GothamCrimProject.DATA
{
    public class CrimeReportMD
    {
        public int CrimeReportID { get; set; }
        [Display(Name ="Crime Reporter")]
        public int CrimeReporterID { get; set; }
        [Display(Name = "Crime Fighter Assigned")]
        public int CrimeFighterID { get; set; }
        [Display(Name = "Suspect of Crime")]
        public Nullable<int> CrimeSuspectID { get; set; }
        [Display(Name = "Supporting Evidence")]
        public string Evidence { get; set; }
        [Display(Name = "Location of Crime")]
        public int CrimeLocationID { get; set; }
        [Display(Name = "Description of Crime")]
        [UIHint("MultilineText")]
        public string CrimeDescription { get; set; }
        [Display(Name = "Crime Reported")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public System.DateTime CrimeTimeSubmission { get; set; }
        [Display(Name = "Crime Resolved")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public Nullable<System.DateTime> CrimeTimeResolved { get; set; }
        [Display(Name = "Crime Status")]
        public int CrimeReportStatusID { get; set; }
    }
    [MetadataType(typeof(CrimeReportMD))]
    public partial class GCCrimeReport {  }
}
