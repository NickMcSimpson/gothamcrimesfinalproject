﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GothamCrimProject.DATA
{
    class LocationMD
    {
        public int LocationID { get; set; }
        [Display(Name ="Area of Gotham")]
        public string LocationZoneName { get; set; }
        [Display(Name ="Description of Zone")]
        [UIHint("MultilineText")]
        public string LocationZoneDescription { get; set; }
    }
    [MetadataType(typeof(LocationMD))]
    public partial class GCLocation { }
}
