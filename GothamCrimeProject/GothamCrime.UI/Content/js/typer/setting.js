(function($) {
	'use strict';	
            var win = $(window),
                foo = $('.typer');

            foo.typer([
                'Hello World',
                'Full Stack Development',
                'MVC5',
                'C#',
                'Cascading Style Sheets',
                'Visual Studio',
                'SQL Server Management',
                'HTML/CSS'
			]);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));
            }).resize();
})(jQuery);