﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GothamCrimProject.DATA;

namespace GothamCrime.UI.Controllers
{
    [Authorize]
    public class GCCrimeReportNotesController : Controller
    {
        private GothamCrimesEntities db = new GothamCrimesEntities();
        
        //public JsonResult AddCRNote(int CRId, string note)
        //{
        //    //Grab the Crime Report
        //    GCCrimeReport cR = db.GCCrimeReports.Single(x => x.CrimeReportID == CRId);
        //    //Get the Employee
        //    GCPerson author = db.GCPersons.ToList().FirstOrDefault(x => x.PersonID == (int)Session["userID"]);

        //    if (author != null)
        //    {
        //        //Create the Note
        //        GCCrimeReportNote newNote = new GCCrimeReportNote()
        //        {
        //            CRNCrimeID = cR.CrimeReportID,
        //            CRNAuthorID = author.PersonID,
        //            CRNDate = DateTime.Now,
        //            CRNote = note
        //        };
        //        db.GCCrimeReportNotes.Add(newNote);
        //        db.SaveChanges();

        //        var data = new
        //        {
        //            CRNote = newNote.CRNote,
        //            Author = newNote.GCPerson.AliasOr,
        //            Date = string.Format("{0:D}", newNote.CRNDate)
        //        };
        //        return Json(data, JsonRequestBehavior.AllowGet);
        //    }
        //    return null;
        //}

        // GET: GCCrimeReportNotes
        public ActionResult Index()
        {
            var gCCrimeReportNotes = db.GCCrimeReportNotes.Include(g => g.GCCrimeReport).Include(g => g.GCPerson);
            return View(gCCrimeReportNotes.ToList());
        }

        // GET: GCCrimeReportNotes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCCrimeReportNote gCCrimeReportNote = db.GCCrimeReportNotes.Find(id);
            if (gCCrimeReportNote == null)
            {
                return HttpNotFound();
            }
            return View(gCCrimeReportNote);
        }

        // GET: GCCrimeReportNotes/Create
        public ActionResult Create(int id)
        {
            ViewBag.CRNCrimeID = new SelectList(db.GCCrimeReports, "CrimeReportID", "Evidence");
            ViewBag.CRNAuthorID = new SelectList(db.GCPersons, "PersonID", "PersonFName");
            //Create the Note
            //GCCrimeReportNote newNote = new GCCrimeReportNote()
            //{
            //    CRNCrimeID = id
            //};
            TempData["passid"] = id;
            return PartialView();
        }

        // POST: GCCrimeReportNotes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CrimeReportNoteID,CRNAuthorID,CRNote,CRNCrimeID,CRNDate")] GCCrimeReportNote gCCrimeReportNote)
        {
            
            if (ModelState.IsValid)
            {
                //Grab the Crime Report
                //GCCrimeReport cR = db.GCCrimeReports.Single(x => x.CrimeReportID == gCCrimeReportNote.CRNCrimeID);
                //Get the Employee
                GCPerson author = db.GCPersons.ToList().FirstOrDefault(x => x.PersonID == (int)Session["userID"]);
                //add these attributes
                if (author != null)
                {
                    gCCrimeReportNote.CRNCrimeID = (int)TempData["passid"];
                    gCCrimeReportNote.CRNAuthorID = author.PersonID;
                    gCCrimeReportNote.CRNDate = DateTime.Now;
                    ////Create the Note
                    //GCCrimeReportNote newNote = new GCCrimeReportNote()
                    //{
                    //    CRNCrimeID = cR.CrimeReportID,
                    //    CRNAuthorID = author.PersonID,
                    //    CRNDate = DateTime.Now,
                    //    CRNote = note
                    //};
                    db.GCCrimeReportNotes.Add(gCCrimeReportNote);
                    db.SaveChanges();
                }

                //db.GCCrimeReportNotes.Add(gCCrimeReportNote);
                //db.SaveChanges();
                //return RedirectToAction("Index");
            }

            ViewBag.CRNCrimeID = new SelectList(db.GCCrimeReports, "CrimeReportID", "Evidence", gCCrimeReportNote.CRNCrimeID);
            ViewBag.CRNAuthorID = new SelectList(db.GCPersons, "PersonID", "PersonFName", gCCrimeReportNote.CRNAuthorID);
            return PartialView(gCCrimeReportNote);
        }

        // GET: GCCrimeReportNotes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCCrimeReportNote gCCrimeReportNote = db.GCCrimeReportNotes.Find(id);
            if (gCCrimeReportNote == null)
            {
                return HttpNotFound();
            }
            ViewBag.CRNCrimeID = new SelectList(db.GCCrimeReports, "CrimeReportID", "Evidence", gCCrimeReportNote.CRNCrimeID);
            ViewBag.CRNAuthorID = new SelectList(db.GCPersons, "PersonID", "PersonFName", gCCrimeReportNote.CRNAuthorID);
            return View(gCCrimeReportNote);
        }

        // POST: GCCrimeReportNotes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CrimeReportNoteID,CRNAuthorID,CRNote,CRNCrimeID,CRNDate")] GCCrimeReportNote gCCrimeReportNote)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gCCrimeReportNote).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CRNCrimeID = new SelectList(db.GCCrimeReports, "CrimeReportID", "Evidence", gCCrimeReportNote.CRNCrimeID);
            ViewBag.CRNAuthorID = new SelectList(db.GCPersons, "PersonID", "PersonFName", gCCrimeReportNote.CRNAuthorID);
            return View(gCCrimeReportNote);
        }

        // GET: GCCrimeReportNotes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCCrimeReportNote gCCrimeReportNote = db.GCCrimeReportNotes.Find(id);
            if (gCCrimeReportNote == null)
            {
                return HttpNotFound();
            }
            return View(gCCrimeReportNote);
        }

        // POST: GCCrimeReportNotes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GCCrimeReportNote gCCrimeReportNote = db.GCCrimeReportNotes.Find(id);
            db.GCCrimeReportNotes.Remove(gCCrimeReportNote);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
