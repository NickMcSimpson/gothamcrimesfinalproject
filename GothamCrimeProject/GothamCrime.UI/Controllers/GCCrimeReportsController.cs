﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GothamCrimProject.DATA;
using Microsoft.AspNet.Identity;

namespace GothamCrime.UI.Controllers
{
    [Authorize]
    public class GCCrimeReportsController : Controller
    {
        private GothamCrimesEntities db = new GothamCrimesEntities();

        // GET: GCCrimeReports
        public ActionResult Index()
        {
            var gCCrimeReports = db.GCCrimeReports.Include(g => g.GCCrimeReportStatu).Include(g => g.GCLocation).Include(g => g.GCPerson).Include(g => g.GCPerson1);
            return View(gCCrimeReports.ToList());
        }

        // GET: GCCrimeReports/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCCrimeReport gCCrimeReport = db.GCCrimeReports.Find(id);
            if (gCCrimeReport == null)
            {
                return HttpNotFound();
            }
            return View(gCCrimeReport);
        }

        // GET: GCCrimeReports/Create
        public ActionResult Create()
        {
            ViewBag.CrimeReportStatusID = new SelectList(db.GCCrimeReportStatus, "CrimeStatusID", "CrimeStatus");
            ViewBag.CrimeLocationID = new SelectList(db.GCLocations, "LocationID", "LocationZoneName");
            ViewBag.CrimeReporterID = new SelectList(db.GCPersons, "PersonID", "AliasOr");
            ViewBag.CrimeFighterID = new SelectList(db.GCPersons, "PersonID", "AliasOr");
            ViewBag.CrimeSuspectID = new SelectList(db.GCPersons.OrderBy(y => y.TrustRating), "PersonID", "AliasOr");
            return View();
        }

        // POST: GCCrimeReports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CrimeReportID,CrimeReporterID,CrimeFighterID,CrimeSuspectID,Evidence,CrimeLocationID,CrimeDescription,CrimeTimeSubmission,CrimeTimeResolved,CrimeReportStatusID")] GCCrimeReport gCCrimeReport,HttpPostedFileBase evidencePhoto)//String[] selectedRoles
        {
            if (ModelState.IsValid)
            {
                #region Default Values For Crime Report

                //We need to Default the Reporter of the Crime as the user:
                int userID = (int)Session["userID"];
                //For now default to Batman
                gCCrimeReport.CrimeReporterID = userID;

                //Default the Assignment to Batman:
                gCCrimeReport.CrimeFighterID = 10;

                //Default the Submission date to the current date
                gCCrimeReport.CrimeTimeSubmission = DateTime.Now;

                //Default the Status to Pending:
                gCCrimeReport.CrimeReportStatusID = 1;

                #endregion

                #region Evidence Upload
                //Not using a default photo here so leave imgName blank if no evidence
                string imgName = "";
                if (evidencePhoto != null)
                {
                    //Get the file Name from the submission
                    imgName = evidencePhoto.FileName;

                    //Take off the extension to verify filetype
                    string ext = imgName.Substring(imgName.LastIndexOf('.'));

                    //Acceptable extensions:
                    string[] goodExts = new string[] { ".png", ".jpg", ".jpeg", ".gif" };

                    //test
                    if (goodExts.Contains(ext))
                    {
                        //rename the file to protect against duplicates
                        imgName = Guid.NewGuid() + ext;

                        //save to webserver
                        evidencePhoto.SaveAs(Server.MapPath("~/Content/img/EvidenceImages/" + imgName));

                    }else
                    {
                        //Extension no good
                        imgName = "";
                    }//test ext
                }//if evidence is not null

                gCCrimeReport.Evidence = imgName;
                
                #endregion


                db.GCCrimeReports.Add(gCCrimeReport);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CrimeReportStatusID = new SelectList(db.GCCrimeReportStatus, "CrimeStatusID", "CrimeStatus", gCCrimeReport.CrimeReportStatusID);
            ViewBag.CrimeLocationID = new SelectList(db.GCLocations, "LocationID", "LocationZoneName", gCCrimeReport.CrimeLocationID);
            ViewBag.CrimeReporterID = new SelectList(db.GCPersons, "PersonID", "AliasOr", gCCrimeReport.CrimeReporterID);
            ViewBag.CrimeFighterID = new SelectList(db.GCPersons, "PersonID", "AliasOr", gCCrimeReport.CrimeFighterID);
            ViewBag.CrimeSuspectID = new SelectList(db.GCPersons, "PersonID", "AliasOr", gCCrimeReport.CrimeSuspectID);
            return View(gCCrimeReport);
        }

        // GET: GCCrimeReports/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCCrimeReport gCCrimeReport = db.GCCrimeReports.Find(id);
            if (gCCrimeReport == null)
            {
                return HttpNotFound();
            }
            ViewBag.CrimeReportStatusID = new SelectList(db.GCCrimeReportStatus, "CrimeStatusID", "CrimeStatus", gCCrimeReport.CrimeReportStatusID);
            ViewBag.CrimeLocationID = new SelectList(db.GCLocations, "LocationID", "LocationZoneName", gCCrimeReport.CrimeLocationID);
            ViewBag.CrimeReporterID = new SelectList(db.GCPersons, "PersonID", "AliasOr", gCCrimeReport.CrimeReporterID);
            ViewBag.CrimeFighterID = new SelectList(db.GCPersons.AsQueryable().Where(x => x.AffiliationID == 3 || x.AffiliationID == 5).OrderByDescending(y => y.TrustRating), "PersonID", "AliasOr", gCCrimeReport.CrimeFighterID);
            ViewBag.CrimeSuspectID = new SelectList(db.GCPersons, "PersonID", "AliasOr", gCCrimeReport.CrimeSuspectID);
            return View(gCCrimeReport);
        }

        // POST: GCCrimeReports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "CrimeReportID,CrimeReporterID,CrimeFighterID,CrimeSuspectID,Evidence,CrimeLocationID,CrimeDescription,CrimeTimeSubmission,CrimeTimeResolved,CrimeReportStatusID")] GCCrimeReport gCCrimeReport)
        {
            if (ModelState.IsValid)
            {
                gCCrimeReport.CrimeReporterID = gCCrimeReport.CrimeReporterID;
                //Change the date resolved if that status is changed to "Resolved"
                if (gCCrimeReport.CrimeReportStatusID == 3)
                {
                    gCCrimeReport.CrimeTimeResolved = DateTime.Now;
                }
                else if (gCCrimeReport.CrimeReportStatusID != 3)
                {
                    gCCrimeReport.CrimeTimeResolved = null;
                }

                db.Entry(gCCrimeReport).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CrimeReportStatusID = new SelectList(db.GCCrimeReportStatus, "CrimeStatusID", "CrimeStatus", gCCrimeReport.CrimeReportStatusID);
            ViewBag.CrimeLocationID = new SelectList(db.GCLocations, "LocationID", "LocationZoneName", gCCrimeReport.CrimeLocationID);
            ViewBag.CrimeReporterID = new SelectList(db.GCPersons, "PersonID", "AliasOr", gCCrimeReport.CrimeReporterID);
            ViewBag.CrimeFighterID = new SelectList(db.GCPersons, "PersonID", "AliasOr", gCCrimeReport.CrimeFighterID);
            ViewBag.CrimeSuspectID = new SelectList(db.GCPersons, "PersonID", "AliasOr", gCCrimeReport.CrimeSuspectID);
            return View(gCCrimeReport);
        }

        // GET: GCCrimeReports/Delete/5
        [Authorize(Roles = "Bat-min,Ally")]// Need changed to Soft Delete
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCCrimeReport gCCrimeReport = db.GCCrimeReports.Find(id);
            if (gCCrimeReport == null)
            {
                return HttpNotFound();
            }
            return View(gCCrimeReport);
        }

        // POST: GCCrimeReports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Bat-min,Ally")]
        public ActionResult DeleteConfirmed(int id)
        {
            GCCrimeReport gCCrimeReport = db.GCCrimeReports.Find(id);
            #region Soft Delete
            //Change the status of the Crime Report when the delete button is pressed
            if (gCCrimeReport.CrimeReportStatusID != 3)
            {
                //If the Investigation has gone cold
                gCCrimeReport.CrimeReportStatusID = 4;
            }
            #endregion
            //db.GCCrimeReports.Remove(gCCrimeReport);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
