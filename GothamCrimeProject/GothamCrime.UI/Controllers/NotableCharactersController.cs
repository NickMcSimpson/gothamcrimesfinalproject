﻿using GothamCrimProject.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace GothamCrime.UI.Controllers
{
    [Authorize]
    public class NotableCharactersController : Controller
    {
        private GothamCrimesEntities db = new GothamCrimesEntities();

        // GET: NotableCharacters
        public ActionResult Index()
        {
            //Here will Show the 3 Links for Villains, Heros, GCPD
            return View();
        }
        //Villains page:
        public ActionResult Villains()
        {
            //Include the data base call from PersonsController
            //Modify it to only show the Villains
            var gCVillains = db.GCPersons.Include(g => g.GCAffiliation).Include(g => g.GCLocation).Include(g => g.GCPersonStatu).AsQueryable().Where(g => g.AffiliationID.ToString() == "1");

            return View(gCVillains.ToList());
        }
        //Villains page:
        public ActionResult GCPD()      //These Will Match the Villains
        {//Include the data base call from PersonsController
            //Modify it to only show the Villains
            var gCLawEnforcement = db.GCPersons.Include(g => g.GCAffiliation).Include(g => g.GCLocation).Include(g => g.GCPersonStatu).AsQueryable().Where(g => g.AffiliationID.ToString() == "5");

            return View(gCLawEnforcement.ToList());
        }
        //Villains page:
        public ActionResult Vigilantes()
        {//Include the data base call from PersonsController
            //Modify it to only show the Villains
            var gCVigilantes = db.GCPersons.Include(g => g.GCAffiliation).Include(g => g.GCLocation).Include(g => g.GCPersonStatu).AsQueryable().Where(g => g.AffiliationID.ToString() == "3");

            return View(gCVigilantes.ToList());
        }
        public ActionResult Details(int? id) //Edit this to make it look Pretty
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCPerson gCPerson = db.GCPersons.Find(id);
            if (gCPerson == null)
            {
                return HttpNotFound();
            }
            return View(gCPerson);
        }
    }
}