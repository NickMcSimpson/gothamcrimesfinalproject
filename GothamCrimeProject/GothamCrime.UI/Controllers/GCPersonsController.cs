﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GothamCrimProject.DATA;
using Microsoft.AspNet.Identity.Owin;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;

namespace GothamCrime.UI.Controllers
{
    [Authorize]
    public class GCPersonsController : Controller
    {
        private GothamCrimesEntities db = new GothamCrimesEntities();

        // GET: GCPersons
        [Authorize(Roles = "Bat-min,Ally,Law Enforcement")]
        public ActionResult Index()
        {
            var gCPersons = db.GCPersons.Include(g => g.GCAffiliation).Include(g => g.GCLocation).Include(g => g.GCPersonStatu);
            return View(gCPersons.ToList());
        }

        // GET: GCPersons/Details/5
        [Authorize(Roles = "Bat-min,Ally,Law Enforcement")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCPerson gCPerson = db.GCPersons.Find(id);
            if (gCPerson == null)
            {
                return HttpNotFound();
            }
            return View(gCPerson);
        }

        // GET: GCPersons/Create
        [Authorize(Roles = "Bat-min")]
        public ActionResult Create()
        {
            ViewBag.AffiliationID = new SelectList(db.GCAffiliations, "AffiliationID", "AffilitationName");
            ViewBag.PersonLocationID = new SelectList(db.GCLocations, "LocationID", "LocationZoneName");
            ViewBag.PersonStatusID = new SelectList(db.GCPersonStatus, "PersonStatusID", "PersonStatusName");
            return View();
        }

        // POST: GCPersons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Bat-min")]
        public ActionResult Create([Bind(Include = "PersonID,PersonFName,PersonLName,PersonAlias,ContactPhone,PersonLocationID,AffiliationID,Occupation,PersonStatusID,PersonNotableFeatures,TrustRating,PersonImage")] GCPerson gCPerson, HttpPostedFileBase personPhoto)//string[] selected roles
        {
            if (ModelState.IsValid)
            {
                //Having all people be created by Batman so no default information
                //This keeps the integrity of the system

                #region Generate a default login
                //Don't need to make a login for Villains

                if (gCPerson.AffiliationID != 1 && gCPerson.PersonStatusID == 1)
                {
                    gCPerson.PersonEmail = (gCPerson.PersonAlias != null ? gCPerson.PersonAlias.ToString() : gCPerson.PersonFName.Substring(0, 1).ToLower() + gCPerson.PersonLName.ToString()) + "@GothamBC.com";
                    //User Manager
                    var userManager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    //create app user
                    var newUser = new ApplicationUser()
                    {
                        UserName = (gCPerson.PersonAlias != null ? gCPerson.PersonAlias.ToString() : gCPerson.PersonFName.Substring(0, 1).ToLower() + gCPerson.PersonLName.ToString()),
                        Email = gCPerson.PersonEmail
                    };

                    var chkUser = userManager.Create(newUser, newUser.UserName + "B@4u");

                    //This Will check to see if the Create works, since I ran into errors with this
                    if (chkUser.Succeeded)
                    {
                        userManager.AddToRole(newUser.Id, "Citizen");
                    }
                }

                #endregion

                #region Enable File Upload
                //Make sure to have default photo
                //default imgName
                string imgName = "defaultPhoto.jpg";

                if (personPhoto != null)
                {
                    //take the file name
                    imgName = personPhoto.FileName;

                    //Check extension
                    //take off ext
                    string ext = imgName.Substring(imgName.LastIndexOf('.'));

                    //Compare against good exts
                    string[] goodExts = new string[] { ".png", ".jpg", ".jpeg", ".gif" };

                    if (goodExts.Contains(ext))
                    {
                        //Rename File
                        imgName = Guid.NewGuid() + ext;

                        //Save file
                        personPhoto.SaveAs(Server.MapPath("~/Content/img/PersonImages/" + imgName));

                    }
                    else
                    {
                        imgName = "defaultPhoto.jpg";
                    }//end ext check
                }//end photo capture
                //assign persons image
                gCPerson.PersonImage = imgName;


                #endregion

                db.GCPersons.Add(gCPerson);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AffiliationID = new SelectList(db.GCAffiliations, "AffiliationID", "AffilitationName", gCPerson.AffiliationID);
            ViewBag.PersonLocationID = new SelectList(db.GCLocations, "LocationID", "LocationZoneName", gCPerson.PersonLocationID);
            ViewBag.PersonStatusID = new SelectList(db.GCPersonStatus, "PersonStatusID", "PersonStatusName", gCPerson.PersonStatusID);
            return View(gCPerson);
        }

        // GET: GCPersons/Edit/5
        [Authorize(Roles = "Bat-min")]
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCPerson gCPerson = db.GCPersons.Find(id);
            if (gCPerson == null)
            {
                return HttpNotFound();
            }
            ViewBag.AffiliationID = new SelectList(db.GCAffiliations, "AffiliationID", "AffilitationName", gCPerson.AffiliationID);
            ViewBag.PersonLocationID = new SelectList(db.GCLocations, "LocationID", "LocationZoneName", gCPerson.PersonLocationID);
            ViewBag.PersonStatusID = new SelectList(db.GCPersonStatus, "PersonStatusID", "PersonStatusName", gCPerson.PersonStatusID);
            return View(gCPerson);
        }

        // POST: GCPersons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Bat-min")]
        public ActionResult Edit([Bind(Include = "PersonID,PersonFName,PersonLName,PersonAlias,ContactPhone,PersonLocationID,AffiliationID,Occupation,PersonStatusID,PersonNotableFeatures,TrustRating,PersonImage")] GCPerson gCPerson, HttpPostedFileBase personPhoto)//string[] selected roles
        {
            if (ModelState.IsValid)
            {
                #region Enable File Upload
                //Make sure to have default photo
                //Image will already have something in it here.

                if (personPhoto != null)
                {
                    //take the file name
                    string imgName = personPhoto.FileName;

                    //Check extension
                    //take off ext
                    string ext = imgName.Substring(imgName.LastIndexOf('.'));

                    //Compare against good exts
                    string[] goodExts = new string[] { ".png", ".jpg", ".jpeg", ".gif" };

                    if (goodExts.Contains(ext))
                    {
                        //Rename File
                        imgName = Guid.NewGuid() + ext;

                        //Save file
                        personPhoto.SaveAs(Server.MapPath("~/Content/img/PersonImages/" + imgName));

                        gCPerson.PersonImage = imgName;
                    }
                }//end photo capture
                 //Hidden Field will hold original



                #endregion

                //#region Fix the Current Data
                //if (gCPerson.AffiliationID != 1 && gCPerson.PersonStatusID == 1)
                //{
                //    gCPerson.PersonEmail = (gCPerson.PersonAlias != null ? gCPerson.PersonAlias.ToString() : gCPerson.PersonFName.Substring(0, 1).ToLower() + gCPerson.PersonLName.ToString()) + "@GothamBC.com";
                //    //User Manager
                //    var userManager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                //    //create app user
                //    var newUser = new ApplicationUser()
                //    {
                //        UserName = (gCPerson.PersonAlias != null ? gCPerson.PersonAlias.ToString() : gCPerson.PersonFName.Substring(0, 1).ToLower() + gCPerson.PersonLName.ToString() + "@GothamBC.com"),
                //        Email = gCPerson.PersonEmail
                //    };

                //    var chkUser = userManager.Create(newUser, newUser.UserName + "B@4u");

                //    //newUser.Id = gCPerson.PersonID.ToString();
                //    //newUser.PhoneNumber = gCPerson.ContactPhone;
                //    if (chkUser.Succeeded)
                //    {
                //    userManager.AddToRole(newUser.Id, "Citizen");
                //    }
                //}
                //#endregion

                db.Entry(gCPerson).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AffiliationID = new SelectList(db.GCAffiliations, "AffiliationID", "AffilitationName", gCPerson.AffiliationID);
            ViewBag.PersonLocationID = new SelectList(db.GCLocations, "LocationID", "LocationZoneName", gCPerson.PersonLocationID);
            ViewBag.PersonStatusID = new SelectList(db.GCPersonStatus, "PersonStatusID", "PersonStatusName", gCPerson.PersonStatusID);
            return View(gCPerson);
        }

        // GET: GCPersons/Delete/5
        [Authorize(Roles = "Bat-min")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCPerson gCPerson = db.GCPersons.Find(id);
            if (gCPerson == null)
            {
                return HttpNotFound();
            }
            return View(gCPerson);
        }

        // POST: GCPersons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Bat-min")]
        public ActionResult DeleteConfirmed(int id)
        {
            GCPerson gCPerson = db.GCPersons.Find(id);
            //As long as status is not set as 'Killed'
            if (gCPerson.PersonStatusID != 3)
            {
                //Changed Status to Deceased
                gCPerson.PersonStatusID = 2;
            }
            //db.GCPersons.Remove(gCPerson);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
