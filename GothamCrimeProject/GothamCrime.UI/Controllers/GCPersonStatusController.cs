﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GothamCrimProject.DATA;

namespace GothamCrime.UI.Controllers
{
    [Authorize(Roles = "Bat-min")]
    public class GCPersonStatusController : Controller
    {
        private GothamCrimesEntities db = new GothamCrimesEntities();

        // GET: GCPersonStatus
        public ActionResult Index()
        {
            return View(db.GCPersonStatus.ToList());
        }

        // GET: GCPersonStatus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCPersonStatu gCPersonStatu = db.GCPersonStatus.Find(id);
            if (gCPersonStatu == null)
            {
                return HttpNotFound();
            }
            return View(gCPersonStatu);
        }

        // GET: GCPersonStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GCPersonStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PersonStatusID,PersonStatusName,PersonStatusDescription")] GCPersonStatu gCPersonStatu)
        {
            if (ModelState.IsValid)
            {
                db.GCPersonStatus.Add(gCPersonStatu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gCPersonStatu);
        }

        // GET: GCPersonStatus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCPersonStatu gCPersonStatu = db.GCPersonStatus.Find(id);
            if (gCPersonStatu == null)
            {
                return HttpNotFound();
            }
            return View(gCPersonStatu);
        }

        // POST: GCPersonStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PersonStatusID,PersonStatusName,PersonStatusDescription")] GCPersonStatu gCPersonStatu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gCPersonStatu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gCPersonStatu);
        }

        // GET: GCPersonStatus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCPersonStatu gCPersonStatu = db.GCPersonStatus.Find(id);
            if (gCPersonStatu == null)
            {
                return HttpNotFound();
            }
            return View(gCPersonStatu);
        }

        // POST: GCPersonStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GCPersonStatu gCPersonStatu = db.GCPersonStatus.Find(id);
            db.GCPersonStatus.Remove(gCPersonStatu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
