﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GothamCrimProject.DATA;

namespace GothamCrime.UI.Controllers
{
    [Authorize(Roles ="Bat-min")]
    public class GCAffiliationsController : Controller
    {
        private GothamCrimesEntities db = new GothamCrimesEntities();

        // GET: GCAffiliations
        public ActionResult Index()
        {
            return View(db.GCAffiliations.ToList());
        }

        // GET: GCAffiliations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCAffiliation gCAffiliation = db.GCAffiliations.Find(id);
            if (gCAffiliation == null)
            {
                return HttpNotFound();
            }
            return View(gCAffiliation);
        }

        // GET: GCAffiliations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GCAffiliations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AffiliationID,AffilitationName,AffilitationDescription")] GCAffiliation gCAffiliation)
        {
            if (ModelState.IsValid)
            {
                db.GCAffiliations.Add(gCAffiliation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gCAffiliation);
        }

        // GET: GCAffiliations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCAffiliation gCAffiliation = db.GCAffiliations.Find(id);
            if (gCAffiliation == null)
            {
                return HttpNotFound();
            }
            return View(gCAffiliation);
        }

        // POST: GCAffiliations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AffiliationID,AffilitationName,AffilitationDescription")] GCAffiliation gCAffiliation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gCAffiliation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gCAffiliation);
        }

        // GET: GCAffiliations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCAffiliation gCAffiliation = db.GCAffiliations.Find(id);
            if (gCAffiliation == null)
            {
                return HttpNotFound();
            }
            return View(gCAffiliation);
        }

        // POST: GCAffiliations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GCAffiliation gCAffiliation = db.GCAffiliations.Find(id);
            db.GCAffiliations.Remove(gCAffiliation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
