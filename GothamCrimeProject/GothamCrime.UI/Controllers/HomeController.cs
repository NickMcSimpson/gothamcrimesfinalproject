﻿using GothamCrime.UI.Models;
using System;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace IdentitySample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }
        public ActionResult BatminFunc()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            if (ModelState.IsValid)
            {


                //Process contact form and send an email

                //create body of email
                string body = string.Format("Name: {0}<br/>Email: {1}<br/>Subject: {2}<br/>Message: {3}",
                        contact.Name, contact.Name, contact.Subject, contact.Message);

                //Create and configure mail message

                MailMessage msg = new MailMessage("no-reply@nickmcsimpson.com", "n.mcsimp@gmail.com", "Contact From Final Project" + contact.Subject, body);
                msg.IsBodyHtml = true;
                msg.Priority = MailPriority.High;

                //Configure SMTP client
                SmtpClient client = new SmtpClient("mail.nickmcsimpson.com");
                client.Credentials = new NetworkCredential("no-reply@nickmcsimpson.com", "l@M2afmwba");

                using (client)
                {
                    try
                    {
                        client.Send(msg);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Fail = ex.StackTrace;
                        ViewBag.ErrorMessage = "There was an error sending your email, please try again.";
                        return View();
                    }
                }//end email send

                //send to confirmation page
                return View("ContactConfirmation", contact);
            }//end if ModelState
            //Otherwise
            return View(contact);

        }//end Contact post


    }
}
