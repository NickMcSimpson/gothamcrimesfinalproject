﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GothamCrimProject.DATA;

namespace GothamCrime.UI.Controllers
{
    [Authorize]
    public class GCLocationsController : Controller
    {
        private GothamCrimesEntities db = new GothamCrimesEntities();

        // GET: GCLocations
        public ActionResult Index()
        {
            return View(db.GCLocations.ToList());
        }

        // GET: GCLocations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCLocation gCLocation = db.GCLocations.Find(id);
            if (gCLocation == null)
            {
                return HttpNotFound();
            }
            return View(gCLocation);
        }

        // GET: GCLocations/Create
        [Authorize(Roles = "Bat-min")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: GCLocations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Bat-min")]
        public ActionResult Create([Bind(Include = "LocationID,LocationZoneName,LocationZoneDescription")] GCLocation gCLocation)
        {
            if (ModelState.IsValid)
            {
                db.GCLocations.Add(gCLocation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gCLocation);
        }

        // GET: GCLocations/Edit/5
        [Authorize(Roles = "Bat-min")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCLocation gCLocation = db.GCLocations.Find(id);
            if (gCLocation == null)
            {
                return HttpNotFound();
            }
            return View(gCLocation);
        }

        // POST: GCLocations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Bat-min")]
        public ActionResult Edit([Bind(Include = "LocationID,LocationZoneName,LocationZoneDescription")] GCLocation gCLocation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gCLocation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gCLocation);
        }

        // GET: GCLocations/Delete/5
        [Authorize(Roles = "Bat-min")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GCLocation gCLocation = db.GCLocations.Find(id);
            if (gCLocation == null)
            {
                return HttpNotFound();
            }
            return View(gCLocation);
        }

        // POST: GCLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Bat-min")]
        public ActionResult DeleteConfirmed(int id)
        {
            GCLocation gCLocation = db.GCLocations.Find(id);
            db.GCLocations.Remove(gCLocation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
